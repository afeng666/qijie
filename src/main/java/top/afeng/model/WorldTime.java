package top.afeng.model;

public class WorldTime {

    private long worldTime;


    public long getWorldTime() {
        return worldTime;
    }

    public void setWorldTime(long worldTime) {
        this.worldTime = worldTime;
    }
}
