package top.afeng;

import top.afeng.model.WorldTime;
import top.afeng.ui.JGameMap;
import top.afeng.ui.JStandardWindow;
import top.afeng.util.Constrant;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameManager {

    private static WorldTime worldTime;

    private static JStandardWindow standardWindow;

    private static JGameMap gameMap;

    public static void main(String[] args) {
        worldTime = new WorldTime();
        worldTime.setWorldTime(System.currentTimeMillis());

        // swing 定位动画是比较繁琐的，不建议从头开始，太浪费时间了，可以找个项目
        gameMap = new JGameMap();
        standardWindow = new JStandardWindow();
        standardWindow.add(gameMap);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            gameMap.repaint();
            System.out.println("worldTime = " + System.currentTimeMillis());
        }, 0, Constrant.FPS, TimeUnit.MILLISECONDS);

    }
}
