package top.afeng.ui;

import javax.swing.*;
import java.awt.*;

public class JStandardWindow extends JFrame {

    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public JStandardWindow() {
        initDefaultWindowStyle();
    }

    private void initDefaultWindowStyle() {
        //建立窗口
        setTitle("java标准窗口");
        this.setSize(screenSize.width/2,screenSize.height/2);//窗口大小
        this.setLocationRelativeTo(null);//窗口显示屏幕中间
        this.setResizable(false);//固定窗口大小
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//设置窗体关闭事件
//        this.add(new GamePanel());//添加游戏内容
        this.setVisible(true);//设置窗体可见
    }

}
