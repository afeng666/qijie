package top.afeng.ui;

import top.afeng.model.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class JGameMap extends JPanel implements MouseListener {

    public JGameMap(){
        initDefaultMapSytle();
    }

    private void initDefaultMapSytle() {
//        setBackground(Color.BLACK);
        setSize(200,200);
        // TODO: 2021/4/17 为啥添加不显示
        add(new Player());
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
